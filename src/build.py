"""
Docstring for module
"""
from pydantic import BaseModel  # pylint: disable=no-name-in-module


class Hello(BaseModel):
    """Model for testing purpose"""

    who: str = "world"

    def main(self):
        """First public method"""
        print(f"Hello {self.who}")

    def second_main(self):
        """Second public method"""


if __name__ == "__main__":
    h = Hello()
    h.main()
